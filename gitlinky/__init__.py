"""Manage large files in git using symlinks.

Symlink files into a .git/linky directory, using a 'filename is hash of
contents' scheme, or 'content-addressable storage' in other words.

---

Example to rewrite history:

  \b
  git filter-branch --tree-filter \\
    'ln -s "$GIT_DIR" .git && git linky add-all -s .jpg && rm .git'


To setup a gpg-encrypted other repo:

  \b
  # First set RECIPIENT to be the key to encrypt to.
  # You can list your keys with `gpg --list-keys`.
  # export RECIPIENT=
  # export TARGET_PATH=/path/to/encrypt/at
  mkdir -p "${TARGET_PATH}" &&
    echo "{\\"gpg-recipient\\": \\"$RECIPIENT\\"}" > "${TARGET_PATH}"/linky.config.json &&
    git config --path --add linky.otherpath "${TARGET_PATH}"


Setup a new clone to sync into:

  \b
  mkdir -p .git/linky/{{0..9},{a..f}}{{0..9},{a..f}}
"""
import hashlib
import json
import logging
import os
import pathlib
import shutil
import stat
import subprocess
import sys

import click


class NoLinkyPathConfigured(click.ClickException):
    pass


@click.group(
    help=__doc__.split("---")[0],
    epilog=__doc__.split("---")[1],
    # Use alternate help option names, since git will capture '--help' and
    # redirect to man-pages.
    context_settings={"help_option_names": ["-h", "--help-text"]},
)
def main():
    pass


def init_logging(verbosity):
    level = logging.INFO
    level = max(level - verbosity * 10, logging.DEBUG)
    logging.basicConfig(format="%(message)s", level=level)


def git_check_output(*args):
    return subprocess.check_output(
        ("git",) + args, universal_newlines=True,
    ).strip()


def git_check_output_list(*args):
    return subprocess.check_output(
        ("git",) + args, universal_newlines=True,
    ).splitlines()


def get_git_root():
    return pathlib.Path(git_check_output("rev-parse", "--show-toplevel"))


def git_ls_files():
    files = subprocess.check_output(
        ["git", "ls-files", "--cached", "--others", "-z"],
        universal_newlines=True,
    ).split("\0")

    result = []
    for f in files:
        path = pathlib.Path(f)
        if not path.exists():
            # Files can be in the index but deleted in the working copy.
            continue

        if path.is_dir():
            # Apparently we can get '.' in the output.
            continue

        result.append(f)
    return result


def hash_file(file_path):
    block_size = 64 * 1024
    file_hash = hashlib.sha1()
    with open(file_path, "rb") as f:
        while True:
            buff = f.read(block_size)
            if not buff:
                break
            file_hash.update(buff)
    return file_hash.hexdigest()


def path_to_absolute(file_path):
    # Resolve a path without resolving the file's symlink.
    # Note that if the parent directory is a symlink then this won't work as
    # expected, if that arrangement is possible.
    #
    return file_path.parent.resolve() / file_path.name


def relative_path_up_to(from_, up_to):
    source = path_to_absolute(from_).relative_to(up_to)
    source_to_root = pathlib.Path("/".join([".."] * (len(source.parents) - 1)))
    return source_to_root


def is_file_writable(path):
    all_write = stat.S_IWGRP | stat.S_IWUSR | stat.S_IWOTH
    path_stat = path.stat().st_mode
    return bool(path_stat & all_write)


def make_file_non_writable(path):
    all_write = stat.S_IWGRP | stat.S_IWUSR | stat.S_IWOTH
    path_stat = path.stat().st_mode
    path_no_write = path_stat & ~all_write
    path.chmod(path_no_write)


class NotALinkyFileError(Exception):
    pass


class Linky:
    def __init__(self, git_root):
        self.git_root = git_root
        self.linky_path = git_root / ".git" / "linky"

    def _make_linky_path_for_hash(self, file_hash):
        return pathlib.Path(".git") / "linky" / file_hash[:2] / file_hash

    def _make_abs_linky_path_for_hash(self, file_hash):
        return self.linky_path / file_hash[:2] / file_hash

    def ensure_linky_structure(self):
        for i in "0123456789abcdef":
            for j in "0123456789abcdef":
                hashdir = self.linky_path / (i + j)
                hashdir.mkdir(parents=True, exist_ok=True)

    def _make_linky_path_for_content(self, file_path):
        file_hash = hash_file(file_path)
        dest_dir = self.linky_path / file_hash[:2]
        dest_dir.mkdir(parents=True, exist_ok=True)
        abs_dest = dest_dir / file_hash
        rel_dest = pathlib.Path(".git") / "linky" / file_hash[:2] / file_hash
        return abs_dest, rel_dest

    def is_linky_file(self, file_path):
        if not file_path.is_symlink():
            return False

        link_path = pathlib.Path(os.readlink(file_path))

        if len(link_path.parts) < 4:
            return False

        if len(link_path.parts[-2]) != 2:
            return False

        if link_path.parts[-3] != "linky":
            return False

        return True

    def add_file(self, file_path):
        file_path = pathlib.Path(file_path)

        if self.is_linky_file(file_path):
            logging.debug(f"Ignored linky-file {file_path}")
            return

        abs_dest, rel_dest = self._make_linky_path_for_content(file_path)

        if not abs_dest.exists():
            if file_path.is_symlink():
                shutil.copyfile(file_path, abs_dest)
            else:
                shutil.move(file_path, abs_dest)
            make_file_non_writable(abs_dest)
            logging.info(f"Stored {rel_dest}")

        link_target = relative_path_up_to(file_path, self.git_root) / rel_dest
        if file_path.exists():
            file_path.unlink()
        file_path.symlink_to(link_target)
        logging.info(f"Added {file_path} -> {link_target}")

    def fix_file(self, file_path):
        file_path = pathlib.Path(file_path)

        if not self.is_linky_file(file_path):
            raise NotALinkyFileError(f"{file_path} is not a linky file.")

        current_link_target = pathlib.Path(os.readlink(file_path))
        file_hash = current_link_target.name
        rel_dest = self._make_linky_path_for_hash(file_hash)
        link_target = relative_path_up_to(file_path, self.git_root) / rel_dest

        if link_target == current_link_target:
            return

        file_path.unlink()
        file_path.symlink_to(link_target)
        logging.info(f"{file_path} -> {link_target}")

    def edit_file(self, file_path):
        file_path = pathlib.Path(file_path)

        if not self.is_linky_file(file_path):
            raise NotALinkyFileError(f"{file_path} is not a linky file.")

        current_link_target = pathlib.Path(os.readlink(file_path))
        file_hash = current_link_target.name
        abs_source = self._make_abs_linky_path_for_hash(file_hash)

        file_path.unlink()
        shutil.copyfile(abs_source, file_path)
        logging.debug(f"{abs_source} -> {file_path}")

    def yield_paths(self):
        chars = "0123456789abcdef"
        for i in chars:
            for j in chars:
                hashdir = self.linky_path / (i + j)
                yield from (path for path in hashdir.iterdir())


def make_linky():
    return Linky(get_git_root())


@main.command()
@click.argument("path", type=click.Path(exists=True, dir_okay=False), nargs=-1)
@click.option("-v", "--verbose", count=True)
def add(path, verbose):
    init_logging(verbose)

    linky = make_linky()
    for p in path:
        linky.add_file(p)


@main.command("add-all")
@click.option("--suffix", "-s", multiple=True)
@click.option("--dry-run", "-n", is_flag=True)
@click.option("-v", "--verbose", count=True)
def add_all(suffix, dry_run, verbose):
    init_logging(verbose)

    linky = make_linky()
    files = git_ls_files()
    for f in files:
        if suffix:
            found = False
            for s in suffix:
                if f.endswith(s):
                    found = True
            if not found:
                continue

        if dry_run:
            click.echo(f)
        else:
            linky.add_file(f)


@main.command()
@click.argument(
    "path", type=click.Path(exists=False, dir_okay=False), nargs=-1
)
@click.option("-v", "--verbose", count=True)
def fix(path, verbose):
    init_logging(verbose)

    linky = make_linky()
    for p in path:
        try:
            linky.fix_file(p)
        except NotALinkyFileError:
            pass


@main.command("fix-all")
@click.option("--suffix", "-s", multiple=True)
@click.option("--dry-run", "-n", is_flag=True)
@click.option("-v", "--verbose", count=True)
def fix_all(suffix, dry_run, verbose):
    init_logging(verbose)

    linky = make_linky()
    files = git_ls_files()
    for f in files:
        if suffix:
            found = False
            for s in suffix:
                if f.endswith(s):
                    found = True
            if not found:
                continue

        if dry_run:
            click.echo(f)
        else:
            try:
                linky.fix_file(f)
            except NotALinkyFileError:
                pass


@main.command()
@click.argument("path", type=click.Path(exists=True, dir_okay=False), nargs=-1)
@click.option("-v", "--verbose", count=True)
def edit(path, verbose):
    init_logging(verbose)

    linky = make_linky()
    for p in path:
        try:
            linky.edit_file(p)
        except NotALinkyFileError as e:
            raise click.ClickException(str(e))


def yield_hashes_from_linky_path(linky_path):

    for hash_dir in linky_path.iterdir():
        if hash_dir.name == "linky.config.json" and hash_dir.is_file():
            continue
        if len(hash_dir.name) != 2:
            raise Exception(f"Does not belong in linky dir: {hash_dir}")
        if not hash_dir.is_dir():
            raise Exception(f"Non-directory in linky dir: {hash_dir}")
        for hashed_file in hash_dir.iterdir():
            if not hashed_file.name.startswith(hash_dir.name):
                raise Exception(f"Hashed file in wrong dir: {hashed_file}")
            if not hashed_file.is_file():
                raise Exception(f"Non-file in hash dir: {hashed_file}")
            yield hashed_file.name


def transfer_linky_hashes(from_path, to_path, hashes, transfer_func):

    for h in hashes:
        rel_path = pathlib.Path(h[:2]) / h
        transfer_func(from_path / rel_path, to_path / rel_path)


def print_transfer_linky_hashes(
    label, from_path, to_path, hashes, transfer_func
):

    if not hashes:
        return

    def item_to_str(item):
        if item is not None:
            return str(item)
        return ""

    with click.progressbar(
        hashes, label=label, show_pos=True, item_show_func=item_to_str
    ) as progress_hashes:

        transfer_linky_hashes(
            from_path, to_path, progress_hashes, transfer_func
        )


def gpg_decrypt(from_path, to_path):
    to_path.parent.mkdir(parents=True, exist_ok=True)
    args = [
        "gpg",
        "--quiet",
        "--output",
        str(to_path),
        "--decrypt",
        str(from_path),
    ]
    subprocess.check_call(args)


def make_gpg_encrypt(recipient):
    def gpg_encrypt(from_path, to_path):
        to_path.parent.mkdir(parents=True, exist_ok=True)
        args = [
            "gpg",
            "--quiet",
            "--recipient",
            recipient,
            "--output",
            str(to_path),
            "--encrypt",
            str(from_path),
        ]
        subprocess.check_call(args)

    return gpg_encrypt


def get_other_linky_path():
    try:
        return pathlib.Path(
            git_check_output("config", "--get", "linky.otherpath")
        )
    except subprocess.CalledProcessError as e:
        if e.returncode != 1:
            raise

    remotes = git_check_output_list("remote")
    if "origin" in remotes:
        origin_path = pathlib.Path(
            git_check_output("remote", "get-url", "origin")
        )
        other_path = origin_path.with_name(origin_path.name + ".linky")
        return other_path

    raise NoLinkyPathConfigured(
        "No linky path configured. "
        'Try "git config --path --add linky.otherpath /path/to/linky".'
    )


@main.command()
@click.option("-v", "--verbose", count=True)
@click.option("--dry-run", "-n", is_flag=True)
@click.option("--no-config-ok", is_flag=True)
def sync(verbose, dry_run, no_config_ok):
    init_logging(verbose)
    linky = make_linky()

    linky.ensure_linky_structure()

    try:
        other_path = get_other_linky_path()
    except NoLinkyPathConfigured:
        if no_config_ok:
            logging.debug("nothing to do: no linky path configured.")
            return
        raise
    our_path = linky.linky_path

    other_config_path = other_path / "linky.config.json"
    other_config = {}
    if other_config_path.exists():
        other_config = json.loads(other_config_path.read_text())

    if "gpg-recipient" in other_config:
        logging.debug(f"remote (GPG): {other_path}")
    else:
        logging.debug(f"remote: {other_path}")

    other_set = set(yield_hashes_from_linky_path(other_path))
    our_set = set(yield_hashes_from_linky_path(our_path))

    logging.debug(
        f"{len(our_set)} local files, {len(other_set)} remote files."
    )

    missing_from_ours = other_set - our_set
    missing_from_other = our_set - other_set

    if not missing_from_ours and not missing_from_other:
        return

    logging.info(
        f"{len(missing_from_ours)} to fetch, "
        f"{len(missing_from_other)} to push."
    )

    fetch_func = shutil.copy
    push_func = shutil.copy
    if "gpg-recipient" in other_config:
        recipient = other_config["gpg-recipient"]
        fetch_func = gpg_decrypt
        push_func = make_gpg_encrypt(recipient)
        click.secho("gpg enabled.", fg="cyan")
        logging.debug(f"recipient: {recipient}")

    if dry_run:
        return

    print_transfer_linky_hashes(
        "fetch", other_path, our_path, missing_from_ours, fetch_func
    )
    print_transfer_linky_hashes(
        "push ", our_path, other_path, missing_from_other, push_func
    )


@main.command()
@click.option("-v", "--verbose", count=True)
@click.option("-f", "--fix", is_flag=True)
def fsck(verbose, fix):
    init_logging(verbose)
    linky = make_linky()
    any_bad = False

    def item_to_str(item):
        if item is not None:
            return str(item)
        return ""

    with click.progressbar(
        list(linky.yield_paths()),
        label="fsck",
        show_pos=True,
        item_show_func=item_to_str,
    ) as progress_paths:
        for path in progress_paths:
            if path.is_dir():
                any_bad = True
                print(file=sys.stderr)
                logging.error(f"Unexpected directory in linky dir: '{path}'.")
            if is_file_writable(path):
                if not fix:
                    any_bad = True
                    print(file=sys.stderr)
                    logging.error(f"File is writable: '{path}'.")
                else:
                    make_file_non_writable(path)
                    print(file=sys.stderr)
                    logging.info(f"Made file non-writable: '{path}'.")
            filehash = hash_file(path)
            if filehash != path.name:
                any_bad = True
                print(file=sys.stderr)
                logging.error(
                    f"Hash does not match filename: '{path}' != {filehash}."
                )
    if any_bad:
        raise click.ClickException("Some bad things found.")


# Copyright 2020 Angelos Evripiotis
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
