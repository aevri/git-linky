#! /bin/bash

trap 'echo Failed.' EXIT
set -e # exit immediately on error

# cd to the root of the repository, so all the paths are relative to that
cd "$(dirname "$0")"/..

allscripts=$(find . -iname '*.py' |  tr '\n' ' ')

# printf '.'
# python3 -m pylint --errors-only .

printf '.'
python3 -m pyflakes $allscripts

printf '.'
python3 -m vulture \
    --exclude 'tests/' \
    --ignore-decorators '@main.command' \
    gitlinky/

echo OK
trap - EXIT
