import fastentrypoints
import setuptools

assert fastentrypoints  # Keep pyflakes happy.


setuptools.setup(
    name="git-linky",
    author="Angelos Evripiotis",
    author_email="angelos.evripiotis@gmail.com",
    zip_safe=False,
    packages=["gitlinky",],
    entry_points={"console_scripts": ["git-linky=gitlinky:main",]},
    install_requires=["click", "colorama"],
    python_requires=">=3.6",
)
